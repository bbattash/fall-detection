import torch
from torch import nn
from models import resnet
from models import resnet, resnet2p1d, pre_act_resnet, wide_resnet, resnext, densenet,resnet_m,mfnet3d,resnet_flow,mfnet3d_top
from models.rgb_r2plus1d import rgb_r2plus1d_64f_34_bert10,rgb_r2plus1d_32f_34_bert10
from models.blocks import AdaptiveConcatPool3d,Flatten
from collections import OrderedDict

def get_module_name(name):
    name = name.split('.')
    if name[0] == 'module':
        i = 1
    else:
        i = 0
    if name[i] == 'features':
        i += 1

    return name[i]

def fix_conv1_weight_of(dict):
    new_model = OrderedDict()
    # get all layer's names from model
    for k,v in dict.items():
        # create new name and update new model
        if "conv1.conv.weight" in k:
            if v.shape[1] == 3:
                new_model[k]=v[:,:2,:,:,:]
            else:
                return dict
        else:
            new_model[k] = v
    return new_model
def get_fine_tuning_parameters(opt,model, ft_begin_module):

    if not ft_begin_module:
        return model.parameters()
    if "top" in  opt.model:
        parameters = []
        add_flag = False
        for k, v in model.module.rgb.named_parameters():
            if ft_begin_module == get_module_name(k):
                add_flag = True

            if add_flag:
                parameters.append({'params': v})

        for k, v in model.module.of.named_parameters():
            #if ft_begin_module == get_module_name(k):
            add_flag = True

            if add_flag:
                parameters.append({'params': v})

    else:
        parameters = []
        add_flag = False
        for k, v in model.named_parameters():
            if ft_begin_module == get_module_name(k):
                add_flag = True

            if add_flag:
                parameters.append({'params': v})

    return parameters


def generate_model(opt):
    assert opt.model in [
        'resnet', 'resnet2p1d', 'preresnet', 'wideresnet', 'resnext', 'densenet','mfnet','resflow','r2plus1','mfnet_top'
    ]
    if opt.model == 'resflow':
        model = resnet_flow.resnet_3d_v1(resnet_depth=50, num_classes=opt.n_classes, data_format='channels_last', is_3d=True, non_local=[0, 0, 0, 0],
                     rep_flow=[0, 0, 0, 0, 0])
    elif opt.model == "r2plus1":
        model = rgb_r2plus1d_32f_34_bert10(opt.n_classes,opt.sample_duration)
    elif opt.model == 'mfnet':
        model = mfnet3d.MFNET_3D(num_classes=opt.n_classes, pretrained=False)
    elif opt.model == 'mfnet_top':
        model = mfnet3d_top.MFNET_3D_TOP(num_classes=opt.n_classes, pretrained=False)
    elif opt.model == 'resnet':
        model = resnet.generate_model(model_depth=opt.model_depth,
                                      n_classes=opt.n_classes,
                                      n_input_channels=opt.n_input_channels,
                                      shortcut_type=opt.resnet_shortcut,
                                      conv1_t_size=opt.conv1_t_size,
                                      conv1_t_stride=opt.conv1_t_stride,
                                      no_max_pool=opt.no_max_pool,
                                      widen_factor=opt.resnet_widen_factor)

    elif opt.model == 'resnet2p1d':
        model = resnet2p1d.generate_model(model_depth=opt.model_depth,
                                          n_classes=opt.n_classes,
                                          n_input_channels=opt.n_input_channels,
                                          shortcut_type=opt.resnet_shortcut,
                                          conv1_t_size=opt.conv1_t_size,
                                          conv1_t_stride=opt.conv1_t_stride,
                                          no_max_pool=opt.no_max_pool,
                                          widen_factor=opt.resnet_widen_factor)
    elif opt.model == 'wideresnet':
        model = wide_resnet.generate_model(
            model_depth=opt.model_depth,
            k=opt.wide_resnet_k,
            n_classes=opt.n_classes,
            n_input_channels=opt.n_input_channels,
            shortcut_type=opt.resnet_shortcut,
            conv1_t_size=opt.conv1_t_size,
            conv1_t_stride=opt.conv1_t_stride,
            no_max_pool=opt.no_max_pool)
    elif opt.model == 'resnext':
        model = resnext.generate_model(model_depth=opt.model_depth,
                                       cardinality=opt.resnext_cardinality,
                                       n_classes=opt.n_classes,
                                       n_input_channels=opt.n_input_channels,
                                       shortcut_type=opt.resnet_shortcut,
                                       conv1_t_size=opt.conv1_t_size,
                                       conv1_t_stride=opt.conv1_t_stride,
                                       no_max_pool=opt.no_max_pool)
    elif opt.model == 'preresnet':
        model = pre_act_resnet.generate_model(
            model_depth=opt.model_depth,
            n_classes=opt.n_classes,
            n_input_channels=opt.n_input_channels,
            shortcut_type=opt.resnet_shortcut,
            conv1_t_size=opt.conv1_t_size,
            conv1_t_stride=opt.conv1_t_stride,
            no_max_pool=opt.no_max_pool)
    elif opt.model == 'densenet':
        model = densenet.generate_model(model_depth=opt.model_depth,
                                        n_classes=opt.n_classes,
                                        n_input_channels=opt.n_input_channels,
                                        conv1_t_size=opt.conv1_t_size,
                                        conv1_t_stride=opt.conv1_t_stride,
                                        no_max_pool=opt.no_max_pool)

    return model

def myhead_func(model):
    model.globalpool = AdaptiveConcatPool3d()
    model.classifier = nn.Sequential(*[Flatten(),
                                       nn.BatchNorm1d(1536),
                                       nn.Dropout(0.2),
                                       nn.Linear(1536, 256, bias=False),
                                       nn.LeakyReLU(inplace=True),
                                       nn.BatchNorm1d(256),
                                       nn.Dropout(0.2),
                                       nn.Linear(256, 32, bias=False),
                                       nn.LeakyReLU(inplace=True),
                                       nn.BatchNorm1d(32),
                                       nn.Linear(32, 2, bias=False),
                                       ])
    return model
def top_load_pretrained_model(model, pretrain_path, model_name, n_finetune_classes, keep_cls, opt):
    from models.mfnet3d_top import  get_cpu_model
    if opt.myhead:
        model.of = myhead_func((model.of))
        model.rgb = myhead_func((model.rgb))

    if pretrain_path:
        print('loading pretrained model {}'.format(pretrain_path))
        try:
            pretrain = torch.load(pretrain_path, map_location='cuda:0')['state_dict']
        except:
            pretrain = torch.load(pretrain_path, map_location='cuda:0')

        if opt.of_pretrain_path:
            print('loading of pretrained model {}'.format(pretrain_path))
            try:
                of_pretrain = torch.load(opt.of_pretrain_path, map_location='cuda:0')['state_dict']
            except:
                of_pretrain = torch.load(opt.of_pretrain_path, map_location='cuda:0')
            model.of.load_state_dict(get_cpu_model(of_pretrain))
        else:##if of weights not specified we will use the rgb ones
            model.of.load_state_dict(fix_conv1_weight_of(get_cpu_model(pretrain)),strict=False)
        model.rgb.load_state_dict(get_cpu_model(pretrain))
        return model
    elif opt.top_pretrain_path:
        try:
            pretrain = torch.load(opt.top_pretrain_path, map_location='cuda:0')['state_dict']
        except:
            pretrain = torch.load(opt.top_pretrain_path, map_location='cuda:0')

        model.load_state_dict(pretrain, strict=False)
        return model

def load_pretrained_model(model, pretrain_path, model_name, n_finetune_classes,keep_cls,opt):
    if pretrain_path:
        print('loading pretrained model {}'.format(pretrain_path))
        try:
            pretrain = torch.load(pretrain_path, map_location='cuda:0')['state_dict']
        except:
            pretrain = torch.load(pretrain_path, map_location='cuda:0')
        #
        # for k,v in pretrain.items():
        #     print(k)
        if not keep_cls:
            if  model_name =="resflow":
                del pretrain['classify.bias']
                del pretrain['classify.weight']
            #elif model_name == "mfnet":
            #    del pretrain['module.classifier.bias']
            #    del pretrain['module.classifier.weight']
        if model_name != "mfnet":
            model.load_state_dict(pretrain,strict=False)

        else:
            from collections import OrderedDict
            new_state_dict = OrderedDict()
            for k, v in pretrain.items():
                if "module" in k:
                    k = k[7:]  # remove `module.`
                new_state_dict[k] = v
            # load params
            if opt.myhead:
                model.globalpool = AdaptiveConcatPool3d()

                model.classifier = nn.Sequential(*[Flatten(),
                                                   nn.BatchNorm1d(1536),
                                                   nn.Dropout(0.2),
                                                   nn.Linear(1536, 256, bias=False),
                                                   nn.LeakyReLU(inplace=True),
                                                   nn.BatchNorm1d(256),
                                                   nn.Dropout(0.2),
                                                   nn.Linear(256, 32, bias=False),
                                                   nn.LeakyReLU(inplace=True),
                                                   nn.BatchNorm1d(32),
                                                   nn.Dropout(0.2),
                                                   nn.Linear(32, 2, bias=False),
                                                   ])
            model.load_state_dict(new_state_dict,strict=False)

        if not keep_cls:

            if model_name == 'densenet':
                model.classifier = nn.Linear(tmp_model.classifier.in_features,
                                                 n_finetune_classes)
            else:
                if model_name == "resflow":
                    fc2layers = False
                    if fc2layers:
                        print("Fc2layers")
                        model.fc = nn.Sequential(nn.Linear(2048, 256),
                                                      nn.ReLU(),
                                                      nn.Dropout(0.2),
                                                      nn.Linear(256, 2))
                    else:
                        model.classify = nn.Linear(2048,2)#nn.Linear(2048,2)#nn.Conv3d(512 * 4, 2, kernel_size=1, stride=1)#

                elif model_name == 'r2plus1':
                    model.fc_action = nn.Linear(512, 2)  # nn.Conv3d(512 * 4, 2, kernel_size=1, stride=1)#

                else:
                    if opt.myhead:
                        model.globalpool = AdaptiveConcatPool3d()

                        model.classifier =nn.Sequential(*[Flatten(),
                                           nn.BatchNorm1d(1536),
                                           nn.Dropout(0.2),
                                           nn.Linear(1536,256, bias=False),
                                           nn.LeakyReLU(inplace=True),
                                           nn.BatchNorm1d(256),
                                           nn.Dropout(0.2),
                                           nn.Linear(256, 32, bias=False),
                                           nn.LeakyReLU(inplace=True),
                                           nn.BatchNorm1d(32),
                                           nn.Linear(32, 2, bias=False),
                                           ])


                    else:
                        model.classifier = nn.Linear(768,2)#resnet.Head()#nn.Linear(tmp_model.fc.in_features,
                                         #n_finetune_classes)

    return model


def make_data_parallel(model, is_distributed, device):
    if is_distributed:
        if device.type == 'cuda' and device.index is not None:
            torch.cuda.set_device(device)
            model.to(device)

            model = nn.parallel.DistributedDataParallel(model,
                                                        device_ids=[device])
        else:
            model.to(device)
            model = nn.parallel.DistributedDataParallel(model)
    elif device.type == 'cuda':
        model = nn.DataParallel(model, device_ids=None).cuda()

    return model
