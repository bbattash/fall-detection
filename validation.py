import torch
import time
import sys
from sklearn.metrics import confusion_matrix
from matplotlib import pyplot as plt
import torch
import torch.distributed as dist

from utils import AverageMeter, calculate_accuracy


def val_epoch(epoch,
              data_loader,
              model,
              criterion,
              device,
              logger,
              tb_writer=None,
              distributed=False):
    print('validation at epoch {}'.format(epoch))

    model.eval()

    batch_time = AverageMeter()
    data_time = AverageMeter()
    losses = AverageMeter()
    accuracies = AverageMeter()
    tp_ = AverageMeter()
    fp_ = AverageMeter()
    fn_ = AverageMeter()
    tn_ = AverageMeter()
    far_ = AverageMeter()
    tpr_ = AverageMeter()
    f1_ = AverageMeter()
    end_time = time.time()
    import matplotlib
    matplotlib.use('TkAgg')
    #fig = plt.figure(figsize=(8,8))
    with torch.no_grad():
        for i, (inputs, targets,path) in enumerate(data_loader):
            data_time.update(time.time() - end_time)

            # for k in range(16):
            #     fig.add_subplot(2, 4, (2*k+1)%8+1)
            #     plt.imshow(inputs[18,:,k,:,:].permute(1, 2, 0))
            #     fig.add_subplot(2,4, (2*k)%8+1)
            #     plt.imshow(inputs[19, :, k, :, :].permute(1, 2, 0))
            #     plt.show()
            targets = targets.to(device, non_blocking=True)
            outputs = model(inputs)
            outputs = outputs.view(outputs.shape[0], outputs.shape[1])

            loss = criterion(outputs, targets)
            acc = calculate_accuracy(outputs, targets)

            #out = torch.softmax(outputs, dim=-1)
            #safty_zone = torch.tensor([0.0, -0.0]).cuda()
            #out = out + safty_zone
            y_pred = torch.argmax(outputs, dim=1).cpu()
            print("pred:   ",y_pred)
            print("targets:",targets)
            # print(y_pred.shape)
            output = confusion_matrix(targets.cpu(), y_pred,labels=[False, True]).ravel()
            if output.shape[0] == 4:
                (tn, fp, fn, tp) = output
            else:
                print("stophere")
            precision = tp / (tp + fp + 1e-7)
            tpr = tp / (tp + fn + 1e-7)
            far = fp / (fp + tn + 1e-7)

            f1 = 2 * (precision * tpr) / (precision + tpr + 1e-7)
            losses.update(loss.item(), inputs.size(0))
            accuracies.update(acc, inputs.size(0))

            batch_time.update(time.time() - end_time)
            end_time = time.time()
            tp_.update(tp, inputs.size(0))
            tn_.update(tn, inputs.size(0))
            fp_.update(fp, inputs.size(0))
            fn_.update(fn, inputs.size(0))
            tpr_.update(tpr, inputs.size(0))
            far_.update(far, inputs.size(0))
            f1_.update(f1, inputs.size(0))
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'Data {data_time.val:.3f} ({data_time.avg:.3f})\t'
                  'Loss {loss.val:.4f} ({loss.avg:.4f})\t'
                  'Acc {acc.val:.3f} ({acc.avg:.3f})\t'
                  'Far {far.val:.3f} ({far.avg:.3f})\t'
                  'Tpr {tpr.val:.3f} ({tpr.avg:.3f})\t'
                  'F1 {f1.val:.3f} ({f1.avg:.3f})'.format(
                      epoch,
                      i + 1,
                      len(data_loader),
                      batch_time=batch_time,
                      data_time=data_time,
                      loss=losses,
                      acc=accuracies,far=far_,tpr=tpr_,f1=f1_))

    if distributed:
        loss_sum = torch.tensor([losses.sum],
                                dtype=torch.float32,
                                device=device)
        loss_count = torch.tensor([losses.count],
                                  dtype=torch.float32,
                                  device=device)
        acc_sum = torch.tensor([accuracies.sum],
                               dtype=torch.float32,
                               device=device)
        acc_count = torch.tensor([accuracies.count],
                                 dtype=torch.float32,
                                 device=device)

        dist.all_reduce(loss_sum, op=dist.ReduceOp.SUM)
        dist.all_reduce(loss_count, op=dist.ReduceOp.SUM)
        dist.all_reduce(acc_sum, op=dist.ReduceOp.SUM)
        dist.all_reduce(acc_count, op=dist.ReduceOp.SUM)

        losses.avg = loss_sum.item() / loss_count.item()
        accuracies.avg = acc_sum.item() / acc_count.item()

    if logger is not None:
        logger.log({'epoch': epoch, 'loss': losses.avg, 'acc': accuracies.avg})

    if tb_writer is not None:
        tb_writer.add_scalar('val/loss', losses.avg, epoch)
        tb_writer.add_scalar('val/acc', accuracies.avg, epoch)

    return losses.avg
