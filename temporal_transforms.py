import random
import math


class Compose(object):

    def __init__(self, transforms):
        self.transforms = transforms

    def __call__(self, frame_indices):
        for i, t in enumerate(self.transforms):
            if isinstance(frame_indices[0], list):
                next_transforms = Compose(self.transforms[i:])
                dst_frame_indices = [
                    next_transforms(clip_frame_indices)
                    for clip_frame_indices in frame_indices
                ]

                return dst_frame_indices
            else:
                frame_indices = t(frame_indices)
        return frame_indices






class TemporalCenterCrop(object):

    def __init__(self, size):
        self.size = size

    def __call__(self, frame_indices):

        center_index = len(frame_indices) // 2
        begin_index = max(0, center_index - (self.size // 2))
        end_index = min(begin_index + self.size, len(frame_indices))

        out = frame_indices[begin_index:end_index]

        for index in out:
            if len(out) >= self.size:
                break
            out.append(index)

        return out


class TemporalRandomCrop(object):

    def __init__(self, size):
        self.size = size

    def __call__(self, frame_indices):
        """
        rand_end = max(0, len(frame_indices) - self.size - 1)
        begin_index = random.randint(0, rand_end)
        end_index = min(begin_index + self.size, len(frame_indices))

        out = frame_indices[begin_index:end_index]

        if len(out) < self.size:
            out = self.loop(out)
        """
        if len(frame_indices) == self.size:
            return frame_indices
        elif len(frame_indices) < self.size:
            delta = -1*(len(frame_indices) - self.size)
            for i in range(delta):
                frame_indices.append(frame_indices[-1])
            return frame_indices
        else:
            delta = len(frame_indices) - self.size
            begin_index =0 #random.choices(list(range(0,delta)))[0]
            out = frame_indices[begin_index:begin_index+self.size]
        return out


class TemporalEventCrop(object):
    def __init__(self, size, n_samples=1,sample_t_stride=1):
        self.size = size
        self.n_samples = n_samples
        self.sample_t_stride = sample_t_stride
        self.subsample = TemporalSubsampling(sample_t_stride)


        #self.loop = LoopPadding(size)

    def __call__(self, total_frame_indices):
        out= []
        for n in range(self.n_samples):
            frame_indices = self.subsample(total_frame_indices)
            if len(frame_indices) == self.size:
                out.append(frame_indices)
            elif len(frame_indices) < self.size:
                delta = -1*(len(frame_indices) - self.size)
                for i in range(delta):
                    frame_indices.append(frame_indices[-1])
                out.append(frame_indices)
            else:
                delta = len(frame_indices) - self.size
                begin_index =random.choices(list(range(0,delta)))[0]
                out.append(frame_indices[begin_index:begin_index+self.size])
        return out

        return out


class SlidingWindow(object):

    def __init__(self, size, stride=0):
        self.size = size
        if stride == 0:
            self.stride = self.size
        else:
            self.stride = stride
        self.loop = LoopPadding(size)

    def __call__(self, frame_indices):
        out = []
        for begin_index in frame_indices[::self.stride]:
            end_index = min(frame_indices[-1] + 1, begin_index + self.size)
            sample = list(range(begin_index, end_index))

            if len(sample) < self.size:
                out.append(self.loop(sample))
                break
            else:
                out.append(sample)

        return out


class TemporalSubsampling(object):

    def __init__(self, stride,train=True):
        self.stride = stride
        self.train = train

    def __call__(self, frame_indices):
        #if self.train:
        start_frame =0# random.choices(list(range(0, self.stride)))[0]
            #print(start_frame)
        return frame_indices[start_frame::self.stride]


class Shuffle(object):

    def __init__(self, block_size):
        self.block_size = block_size

    def __call__(self, frame_indices):
        frame_indices = [
            frame_indices[i:(i + self.block_size)]
            for i in range(0, len(frame_indices), self.block_size)
        ]
        random.shuffle(frame_indices)
        frame_indices = [t for block in frame_indices for t in block]
        return frame_indices