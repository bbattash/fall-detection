import csv
import random
from functools import partialmethod
import cv2
import numpy as np
import torch
import sys
sys.path.append('/home/barak/workspace/RAFT/core/utils')
import flow_viz
import torch
import numpy as np
from sklearn.metrics import precision_recall_fscore_support


class AverageMeter(object):
    """Computes and stores the average and current value"""

    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class Logger(object):

    def __init__(self, path, header):
        self.log_file = path.open('w')
        self.logger = csv.writer(self.log_file, delimiter='\t')

        self.logger.writerow(header)
        self.header = header

    def __del(self):
        self.log_file.close()

    def log(self, values):
        write_values = []
        for col in self.header:
            assert col in values
            write_values.append(values[col])

        self.logger.writerow(write_values)
        self.log_file.flush()


def calculate_accuracy(outputs, targets):
    with torch.no_grad():
        batch_size = targets.size(0)
        out = torch.softmax(outputs,dim=-1)
        #safty_zone = torch.tensor([0.0,0.0]).cuda()
        #out = out + safty_zone
        _, pred = out.topk(1, 1, largest=True, sorted=True)
       # _, pred = outputs.topk(1, 1, largest=True, sorted=True)
        pred = pred.t()
        correct = pred.eq(targets.view(1, -1))
        n_correct_elems = correct.float().sum().item()

        return n_correct_elems / batch_size


def calculate_precision_and_recall(outputs, targets, pos_label=1):
    with torch.no_grad():
        _, pred = outputs.topk(1, 1, largest=True, sorted=True)
        precision, recall, _, _ = precision_recall_fscore_support(
            targets.view(-1, 1).cpu().numpy(),
            pred.cpu().numpy())

        return precision[pos_label], recall[pos_label]


def worker_init_fn(worker_id):
    torch_seed = torch.initial_seed()

    random.seed(torch_seed + worker_id)

    if torch_seed >= 2**32:
        torch_seed = torch_seed % 2**32
    np.random.seed(torch_seed + worker_id)


def get_lr(optimizer):
    lrs = []
    for param_group in optimizer.param_groups:
        lr = float(param_group['lr'])
        lrs.append(lr)

    return max(lrs)


def partialclass(cls, *args, **kwargs):

    class PartialClass(cls):
        __init__ = partialmethod(cls.__init__, *args, **kwargs)

    return PartialClass

def vizualize_flow(img, flo, save=True, counter=0):
    # permute the channels and change device is necessary
    img = img[0].permute(1, 2, 0).cpu().numpy()#*225).astype(np.uint8)
    import matplotlib
    from matplotlib import pyplot as plt
    matplotlib.use('TkAgg')

    flo = flo[0].permute(1, 2, 0).cpu().numpy()

    # map flow to rgb image
    flo = flow_viz.flow_to_image(flo)
    #flo = cv2.cvtColor(flo, cv2.COLOR_RGB2BGR)
    img_flo = np.concatenate([img, flo], axis=0)/255
    plt.imshow(img_flo)
    #plt.imshow(flo/255)
    plt.show()


    # concatenate, save and show images
    img_flo = np.concatenate([img, flo], axis=0)
    if save:
        cv2.imwrite(f"demo_frames/frame_{str(counter)}.jpg", img_flo)
    #cv2.imshow("Optical Flow", img_flo / 255.0)
    #k = cv2.waitKey(25) & 0xFF
    #if k == 27:
    #    return False
    #return True
