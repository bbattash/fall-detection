import json
import copy
import functools
import numpy as np
import torch
from torch.utils.data.dataloader import default_collate

from .videodataset import VideoDataset,TopVideoDataset
def top_collate_fn(batch):
    batch_of_clips, batch_rgb_clips,batch_targets,batch_paths_temp = zip(*batch)

    batch_of_clips = [clip for multi_clips in batch_of_clips for clip in multi_clips]
    batch_rgb_clips = [clip for multi_clips in batch_rgb_clips for clip in multi_clips]

    batch_targets = [
        target for multi_targets in batch_targets for target in multi_targets
    ]
    batch_paths=[]
    for path in batch_paths_temp:
        batch_paths.append(path)
        batch_paths.append(path)
        #batch_paths.append(path)

    target_element = batch_targets[0]
    if isinstance(target_element, int) or isinstance(target_element, str):
        return default_collate(batch_of_clips),default_collate(batch_rgb_clips), default_collate(batch_targets),batch_paths#default_collate(batch_paths)
    else:
        return default_collate(batch_of_clips),default_collate(batch_rgb_clips), batch_targets,batch_paths

def collate_fn(batch):
    batch_clips, batch_targets,batch_paths_temp = zip(*batch)

    batch_clips = [clip for multi_clips in batch_clips for clip in multi_clips]
    batch_targets = [
        target for multi_targets in batch_targets for target in multi_targets
    ]
    batch_paths=[]
    for path in batch_paths_temp:
        batch_paths.append(path)
        batch_paths.append(path)
        #batch_paths.append(path)

    target_element = batch_targets[0]
    if isinstance(target_element, int) or isinstance(target_element, str):
        return default_collate(batch_clips), default_collate(batch_targets),batch_paths#default_collate(batch_paths)
    else:
        return default_collate(batch_clips), batch_targets,batch_paths


class VideoDatasetMultiClips(VideoDataset):

    def __loading(self, path, video_frame_indices):

        clips = []
        segments = []
        for clip_frame_indices in video_frame_indices:
            clip = self.loader(path, clip_frame_indices)
            if self.spatial_transform is not None:
                self.spatial_transform.randomize_parameters()
                clip = [self.spatial_transform(img) for img in clip]
            gap = self.sample_duration - len(clip)
            for frame in range(gap):
                clip.append(clip[-1])
            clips.append(torch.stack(clip, 0).permute(1, 0, 2, 3))
            segments.append(
                [min(clip_frame_indices),
                 max(clip_frame_indices) + 1])

        return clips, segments

    def __getitem__(self, index):
        path = self.data[index]['video']

        video_frame_indices = self.data[index]['frame_indices']
        if self.temporal_transform is not None:
            video_frame_indices = self.temporal_transform(video_frame_indices)

        clips, segments = self.__loading(path, video_frame_indices)

        if isinstance(self.target_type, list):
            target = [self.data[index][t] for t in self.target_type]
        else:
            target = self.data[index][self.target_type]

        if 'segment' in self.target_type:
            if isinstance(self.target_type, list):
                segment_index = self.target_type.index('segment')
                targets = []
                for s in segments:
                    targets.append(copy.deepcopy(target))
                    targets[-1][segment_index] = s
            else:
                targets = segments
        else:
            targets = [target for _ in range(len(segments))]

        return clips, targets,path
class TopVideoDatasetMultiClips(TopVideoDataset):

    def __loading(self, path, video_frame_indices):

        rgb_clips = []
        of_clips = []
        segments = []
        for clip_frame_indices in video_frame_indices:
            clip = self.loader(path, clip_frame_indices)
            if self.spatial_transform is not None:
                self.spatial_transform.randomize_parameters()
                clip = [self.spatial_transform(img) for img in clip]
                rgb_clip = clip.copy()
                rgb_clip = [self.top_spatial_transform(img) for img in rgb_clip]
            gap = self.sample_duration - len(clip)
            for frame in range(gap):
                clip.append(clip[-1])
                rgb_clip.append(rgb_clip[-1])

            rgb_clip = torch.stack(rgb_clip, 0).permute(1, 0, 2, 3)
            clip = [np.asarray(im) for im in clip]

            rgb_clips.append(rgb_clip)
            of_clips.append(clip)
            segments.append(
                [min(clip_frame_indices),
                 max(clip_frame_indices) + 1])

        return of_clips,rgb_clips, segments

    def __getitem__(self, index):
        path = self.data[index]['video']

        video_frame_indices = self.data[index]['frame_indices']
        if self.temporal_transform is not None:
            video_frame_indices = self.temporal_transform(video_frame_indices)

        of_clip,rgb_clip , segments= self.__loading(path, video_frame_indices)

        if isinstance(self.target_type, list):
            target = [self.data[index][t] for t in self.target_type]
        else:
            target = self.data[index][self.target_type]

        if 'segment' in self.target_type:
            if isinstance(self.target_type, list):
                segment_index = self.target_type.index('segment')
                targets = []
                for s in segments:
                    targets.append(copy.deepcopy(target))
                    targets[-1][segment_index] = s
            else:
                targets = segments
        else:
            targets = [target for _ in range(len(segments))]

        return of_clip, rgb_clip, targets,path