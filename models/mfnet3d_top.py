"""
Author: Yunpeng Chen
"""
import logging
import os
from collections import OrderedDict

import torch.nn as nn

from models.mfnet3d import MFNET_3D
from models.mfnet3d_of import MFNET_3D_OF
import sys
import torch
from utils import vizualize_flow
sys.path.append('/home/barak/workspace/RAFT/core')
from raft import RAFT
sys.path.append('/home/barak/workspace/RAFT/core/utils')

from  utils_raft import InputPadder, forward_interpolate

def get_cpu_model(model):
    new_model = OrderedDict()
    # get all layer's names from model
    for name in model:
        # create new name and update new model
        if "module" in name:
            new_name = name[7:]
        else:
            new_name = name
        new_model[new_name] = model[name]
    return new_model


class MFNET_3D_TOP(nn.Module):

    def __init__(self, num_classes, pretrained=False, **kwargs):
        super(MFNET_3D_TOP, self).__init__()
        args = {'model':'/home/barak/workspace/RAFT/models/raft-sintel.pth','dataset':'sintel','small':False,'mixed_precision':False,'alternate_corr':True}


        self.opticflow_extractor  =RAFT(args)
        self.opticflow_extractor.load_state_dict(get_cpu_model(torch.load(args['model'])))
        self.rgb           = MFNET_3D(num_classes)
        self.of            = MFNET_3D_OF(num_classes)


        #############
        # Initialization
        #initializer.xavier(net=self)



    def forward(self, x,x_rgb):
        assert x_rgb.shape[2] == 16
        self.opticflow_extractor.eval()
        with torch.no_grad():
            for k in range(15):
                frame1 = torch.squeeze(x[k]).permute(0,3, 1, 2).float().cuda()
                frame2 = torch.squeeze(x[k+1]).permute(0,3, 1, 2).float().cuda()
                _, flow = self.opticflow_extractor(frame1,frame2,iters=20, test_mode=True)
                if k==0:
                    flow_tensor = torch.unsqueeze(flow,dim=2)
                else:
                    flow_tensor = torch.cat([flow_tensor,torch.unsqueeze(flow,dim=2)],dim=2)
                #ret = vizualize_flow(frame1, flow, save=True, counter=k)
        out = self.rgb(x_rgb)

        out_of=self.of(flow_tensor.detach())
        output = (out +out_of)/2

        return output

if __name__ == "__main__":
    import torch
    logging.getLogger().setLevel(logging.DEBUG)
    # ---------
    net = MFNET_3D(num_classes=100, pretrained=False)
    data = torch.autograd.Variable(torch.randn(1,3,16,224,224))
    output = net(data)
    torch.save({'state_dict': net.state_dict()}, './tmp.pth')
    print (output.shape)